# README #

### How do I get set up? ###

* open a terminal window, and naviagate to the ai_gameplayingagent/src directory. 
* run javac *.java
* open another terminal window, and navigate to the ai_gameplayingagent/src directory
* run 'java Raft -p 31415 -i ../input/s0.in' in one window
* run 'java Agent -p 31415' in the other window

note: the input file (s0.in) can be replaced with any input file s0-s7.in, or similar custom made map. A different port can also be used.

### Who do I talk to? ###

Simon Levitt
simon.levitt@student.unsw.edu.au