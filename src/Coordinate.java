/*********************************************
 *  Coordinate.java 
 *  Agent for Text-Based Adventure Game
 *  COMP3411 Artificial Intelligence
 *  UNSW Session 1, 2017
 *  Made by z5084643 Simon Levitt and z3461102 Daniel Thanh Thuan Nguyen
 ********************************************/

public class Coordinate {
	private int x;
	private int y;
	private char direction;
	
	public Coordinate(int x, int y){
		this.x = x;
		this.y = y;
		direction = '^';
	}
	
	public void setX(int x){
		this.x = x;
	}
	
	public void setY(int y){
		this.y = y;
	}
	
	public void setDirection(char direction){
		this.direction = direction;
	}
	
	public char getDirection(){
		return direction;
	}
	
	public int getX(){
		return x;
	}
	
	public int getY(){
		return y;
	}
	
	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Coordinate)) return false;

        Coordinate that = (Coordinate) o;

        return 	this.y == that.y &&
                this.x == that.x &&
                this.direction == that.direction;
    }
}