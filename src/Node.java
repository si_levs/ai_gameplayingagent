/*********************************************
 *  Node.java 
 *  Agent for Text-Based Adventure Game
 *  COMP3411 Artificial Intelligence
 *  UNSW Session 1, 2017
 *  Made by z5084643 Simon Levitt and z3461102 Daniel Thanh Thuan Nguyen
 ********************************************/

import java.util.Arrays;
import java.util.Objects;

public class Node implements Comparable<Node>{
	private char state[][];
	private char action;
	private double g;
	private double f;
	private Coordinate agentLocation;
	private Boolean haveAxe;
	private Boolean haveBomb;
	private Boolean haveKey;
	private Boolean haveTreasure;
	private Boolean haveRaft;
	private Boolean openedDoor;
	private Boolean addedTool;
	private Boolean onWater;

	/**
	 * Node Constructor (State, agentLocation, action, axe, bomb, key, treasure, raft, addedTool, openedDoor)
	 * @param state
	 * @param agentLocation
	 * @param action
	 * @param haveAxe
	 * @param haveBomb
	 * @param haveKey
	 * @param haveTreasure
	 * @param haveRaft
	 * @param addedTool
	 * @param openedDoor
	 */
	public Node(char state[][], Coordinate agentLocation, char action, 
			Boolean haveAxe, Boolean haveBomb, Boolean haveKey, Boolean haveTreasure, Boolean haveRaft, Boolean addedTool, Boolean openedDoor, 
			Boolean onWater){
		this.state = state;
		this.action = action;
		this.agentLocation = agentLocation;
		this.haveAxe = haveAxe;
		this.haveBomb = haveBomb;
		this.haveKey = haveKey;
		this.haveTreasure = haveTreasure;
		this.haveRaft = haveRaft;
		this.addedTool = addedTool;
		this.openedDoor = openedDoor;
		this.onWater = onWater;
		g = f = Double.POSITIVE_INFINITY;
	}
	
	public char[][] getState(){
		return state;
	}

	public void setG(double g){
		this.g = g;
	}
	
	public double getG(){
		return g;
	}
	
	public void setF(double f){
		this.f = f;
	}
	
	public double getF(){
		return f;
	}
	
	public int getX(){
		return agentLocation.getX();
	}
	
	public int getY(){
		return agentLocation.getY();
	}
	
	public void setX(int x){
		this.agentLocation.setX(x);
	}
	
	public void setY(int y){
		this.agentLocation.setX(y);
	}
	
	public Coordinate getAgentLocation(){
		return agentLocation;
	}
	
	public char getAction(){
		return action;
	}
	
	public void setAction(char action){
		this.action = action;
	}
	
	public char getDirection(){
		return agentLocation.getDirection();
	}
	
	public boolean getHaveAxe(){
		return haveAxe;
	}
	
	public boolean getHaveKey(){
		return haveKey;
	}
	
	public boolean getHaveBomb(){
		return haveBomb;
	}
	
	public boolean getHaveTreasure(){
		return haveTreasure;
	}
	
	public boolean getHaveRaft(){
		return haveRaft;
	}
	
	public void setOnWater(boolean onWater){
		this.onWater = onWater;
	}
	
	public boolean getOnWater(){
		return onWater;
	}
	
	public boolean addedTool(){
		return addedTool;
	}
	
	public void setAddedTool(boolean addedTool){
		this.addedTool = addedTool;
	}
	
	public void setOpenedDoor(boolean openedDoor){
		this.openedDoor = openedDoor;
	}
	
	public boolean getOpenedDoor(){
		return openedDoor;
	}
	
	public char[][] copyState(int xMin, int xMax, int yMin, int yMax){
		char[][] copiedState = new char[state.length][state.length];
		for(int y = yMin; y <= yMax; y++){
			for(int x = xMin; x <= xMax; x++){
				copiedState[y][x] = state[y][x];
			}
		}
		return copiedState;
	}

    @Override
    public int hashCode() {
        return Objects.hash(g, state, agentLocation, haveAxe, haveBomb, haveKey, haveTreasure, haveRaft);
    }
    
    @Override
    public boolean equals(Object o) {

        if (o == this) return true;
        
        if (!(o instanceof Node)) return false;
        
        Node node = (Node) o;

        return 	g >= node.g &&
        		haveRaft == node.haveRaft &&
        		haveKey == node.haveKey &&
        		haveBomb == node.haveBomb &&
        		haveTreasure == node.haveTreasure &&
        		openedDoor == node.openedDoor &&
        		addedTool == node.addedTool &&
        		agentLocation.equals(node.agentLocation) &&
        		Arrays.deepEquals(state, node.state);	
    }

	@Override
	public int compareTo(Node n){
		if(this.getF() == n.getF()) return (int) (n.getG() - this.getG());
		return (int) (this.getF() - n.getF());
	}
	
}
