/*
*	Heuristic implementation.
*/

//	returns manhattan distance * cheapCost to home if haveTreasure, or treasureLocation == null
//	returns manhattan distance * cheapCost to treasure, then back home from treasure location otherwise
public class Heuristic1 implements Heuristic {

	public int getHeuristic(Node start, Coordinate treasureLocation, Coordinate startLocation, int cheapCost) {
		if(start.getHaveTreasure() || treasureLocation == null) return cheapCost*(Math.abs(startLocation.getX()-start.getX()) + Math.abs(startLocation.getY()-start.getY()));
		else return cheapCost*(Math.abs(treasureLocation.getX()-startLocation.getX()) + Math.abs(treasureLocation.getY()-startLocation.getY()) + 
				Math.abs(treasureLocation.getX()-start.getX()) + Math.abs(treasureLocation.getY()-start.getY()));
	}

}
