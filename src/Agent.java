/*********************************************
 *  Agent.java 
 *  Agent for Text-Based Adventure Game
 *  COMP3411 Artificial Intelligence
 *  UNSW Session 1, 2017
 *  Made by z5084643 Simon Levitt and z3461102 Daniel Thanh Thuan Nguyen
 */

import java.util.*;
import java.io.*;
import java.net.*;

public class Agent {
	
	private Heuristic h;
	private Boolean haveAxe;
	private Boolean haveBomb;
	private Boolean haveKey;
	private Boolean haveTreasure;
	private Boolean haveRaft;
	private Boolean onWater;
	private Coordinate treasureLocation;
	private Coordinate startLocation;
	private Coordinate agentLocation;
	private int moveCost;
	private int cheapCost;
	private int xMin;
	private int xMax;
	private int yMin;
	private int yMax;
	private int multiplyCost;
	private char map[][];
	
	/**
	 * Constructor for Agent class
	 */
	public Agent(Heuristic h){
		haveAxe = false;
		haveBomb = false;
		haveKey = false;
		haveTreasure = false;
		haveRaft = false;
		onWater = false;
        map = new char[160][160];
        treasureLocation = null;
        agentLocation = new Coordinate(80,80);
        agentLocation.setDirection('^');
        startLocation = new Coordinate(80,80);
        this.yMax = 82;
        this.yMin = 78;
        this.xMax = 82;
        this.xMin = 78;
        moveCost = 100;
        cheapCost = 10;
        multiplyCost = 1000;
        this.h = h;
	}
	
	
	/**
	 * getAction method that takes in the 5x5 view, updates the current visible map with it, then runs A* on possible actions to take.
	 * @param view
	 * @return action
	 */
	public char get_action(char view[][]) {
		updateMap(view);
		System.out.println(toString(map));
		PriorityQueue<Node> closed = new PriorityQueue<Node>();
		PriorityQueue<Node> open = new PriorityQueue<Node>();
		
		Node current = null;
		Node start = new Node(map, agentLocation, ' ', haveAxe, haveBomb, haveKey, haveTreasure, haveRaft, false, false, onWater);
		
		open.add(start);

		HashMap<Node, Node> cameFrom = new HashMap<Node, Node>();
		HashMap<Node, Double> gScore = new HashMap<Node, Double>();

		gScore.put(start, 0.0);
		start.setF(getHeuristic(start));
		start.setG(0);
		Node exploredMore = null;
		
		while(! open.isEmpty()){
			current = open.poll();

			if( ! haveTreasure ){
				if(current.getHaveTreasure()) return initialAction(current, cameFrom);
			} else {
				if(current.getAgentLocation().equals(startLocation)) return initialAction(current, cameFrom);
			}
			if(current.getOpenedDoor()) return initialAction(current, cameFrom);
			if(current.addedTool()) return initialAction(current, cameFrom);
			if(exploredMore == null && exploredMore(current)) exploredMore = current;
			
			closed.add(current);
			
			LinkedList<Node> neighbours = getNeighbours(current);
			
			for(Node neighbour : neighbours){
				
				if(closed.contains(neighbour)) continue; // Ignore previously expanded nodes
				
				if(gScore.get(neighbour) == null) gScore.put(neighbour, Double.POSITIVE_INFINITY); // Min g() recorded thus far of each node is set to MAX by default
				
				if(! open.contains(neighbour)){ // New node to explore
					open.add(neighbour);
				} else if (neighbour.getG() >= gScore.get(neighbour)){ // This path is equal to, or worse than one previously explored so ignore it
					continue;
				}

				cameFrom.put(neighbour, current); // Save most efficient route to <neighbour> thus far
				gScore.put(neighbour, neighbour.getG()); // Smallest actual cost path to reach <neighbour> found thus far
				
			}
		}
		
		if(exploredMore != null) return initialAction(exploredMore, cameFrom);	
		else{
			Node lowest = null;
			for(Node node : closed){
				if(lowest == null || node.getG() < lowest.getG()) lowest = node;
			}
			if(lowest != null) return initialAction(lowest, cameFrom);
		}
		
		System.out.println("Could not solve.");
		System.exit(-1);
		return 0;
	}
	
	
	/**
	 * exploredMore method determines if the given nodes adds visibility to the overall map
	 * @param current
	 * @return boolean
	 */
	public boolean exploredMore(Node current){
		char inFront = '?';
		char inFront2 = '?';
		int inFrontX = getInFrontX(current.getAgentLocation(), current.getDirection());
		int inFrontY = getInFrontY(current.getAgentLocation(), current.getDirection());
		int inFrontX2 = get2InFrontX(current.getAgentLocation(), current.getDirection());
		int inFrontY2 = get2InFrontY(current.getAgentLocation(), current.getDirection());
		
		try{
			inFront = current.getState()[inFrontY][inFrontX];
			inFront2 = current.getState()[inFrontY2][inFrontX2];
		} catch (ArrayIndexOutOfBoundsException e){
			
		}
		
		if(inFrontX < 0 || inFrontY < 0) return false;
		if(inFront == '?' || inFront == 0 || inFront2 == '?' || inFront2 == 0) return true;
		return false;
	}
	
	
	/**
	 * initialAction determines the initial action needed to be taken that leads to the chosen point of interest (path chosen)
	 * @param current
	 * @param cameFrom
	 * @return action
	 */
	public char initialAction(Node current, HashMap<Node, Node> cameFrom){

		while(cameFrom.keySet().contains(current) && cameFrom.get(current).getAction() != ' '){
			if(cameFrom.get(current) != null){
				current = cameFrom.get(current);
			}
		}

		updateInfo(current);
		return current.getAction();
	}
	
	
	/**
	 * updateInfo updates the agents fields with the chosen actions fields (state after the action is chosen)
	 * @param current
	 */
	public void updateInfo(Node current){

		haveRaft = current.getHaveRaft();
		haveAxe = current.getHaveAxe();
		haveKey = current.getHaveKey();
		haveTreasure = current.getHaveTreasure();
		haveBomb = current.getHaveBomb();
		map = current.copyState(xMin, xMax, yMin, yMax);
		onWater = current.getOnWater();
		agentLocation = current.getAgentLocation();
	}
	
	/**
	 * getInFront element returns the char that is in front of the agent, in the direction of the agent.
	 * @param state
	 * @param location
	 * @return char
	 */
	public char getInFrontElement(char[][] state, Coordinate location){
		return state[getInFrontY(location, location.getDirection())][getInFrontX(location, location.getDirection())];
	}
	
	
	/**
	 * getNeighbours returns a list of neighbours (after taking allowable options LRFCUB)
	 * @param current
	 * @return neighbours
	 */
	public LinkedList<Node> getNeighbours(Node current){
		char[] options = {'L', 'R', 'F', 'C', 'U', 'B'};
		LinkedList<Node> neighbours = new LinkedList<Node>();
		
		for(char option: options){
			switch(option){
				case 'F':
					Node fNeighbour = getFNeighbour(current);
					if(fNeighbour != null) neighbours.add(fNeighbour);
					break;
				case 'R':
					Node rNeighbour = getRNeighbour(current);
					if(rNeighbour != null) neighbours.add(rNeighbour);
					break;
				case 'L':
					Node lNeighbour = getLNeighbour(current);
					if(lNeighbour != null) neighbours.add(lNeighbour);
					break;
				case 'C':
					Node cNeighbour = getCNeighbour(current);
					if(cNeighbour != null) neighbours.add(cNeighbour);
					break;
				case 'U':
					Node uNeighbour = getUNeighbour(current);
					if(uNeighbour != null) neighbours.add(uNeighbour);
					break;
				case 'B':
					Node bNeighbour = getBNeighbour(current);
					if(bNeighbour != null) neighbours.add(bNeighbour);
					break;
			}
		}

		return neighbours;
	}
	
	
	/** 
	 * all get Neighbour methods return a child node of <current>, with their own state, if said action was taken
	 * @param current
	 * @return associated neighbour
	 */
	private Node getBNeighbour(Node current) {

		char inFront = '?';
		char inFront2 = '?';
		int inFrontX = getInFrontX(current.getAgentLocation(), current.getDirection());
		int inFrontY = getInFrontY(current.getAgentLocation(), current.getDirection());
		int inFrontY2 =get2InFrontY(current.getAgentLocation(), current.getDirection());
		int inFrontX2 = get2InFrontX(current.getAgentLocation(), current.getDirection());
		
		try{
			inFront = current.getState()[inFrontY][inFrontX];
			inFront2 = current.getState()[inFrontY2][inFrontX2];
		} catch (ArrayIndexOutOfBoundsException e){
			// leave inFront as '.' (out of bounds)
		}
		
		if(! current.getHaveBomb() || (! (inFront == '*') && ! (inFront == 'T')) && ! (inFront == '-')){
			return current;
		}
		double cost = current.getG() + multiplyCost*moveCost;

		if(inFront == '-' && current.getHaveKey()) cost = Double.POSITIVE_INFINITY;
		if(inFront == 'T' && current.getHaveAxe()) cost = Double.POSITIVE_INFINITY;
		else if(inFront == 'T') cost += multiplyCost * moveCost;
		else if(inFront == '-') cost += multiplyCost * moveCost;
		else if(current.getOnWater()) cost += multiplyCost*moveCost;
		
		if( (inFront2 == 'T' && ! current.getHaveAxe()) || (inFront2 == '~' && ! current.getHaveRaft()) || inFront2 == '*' || 
				inFront2 == '-' && ! current.getHaveKey()){
			cost = Double.POSITIVE_INFINITY;
		}		
		
		char[][] view = current.copyState(xMin, xMax, yMin, yMax);
		
		view[inFrontY][inFrontX] = ' ';
	    Coordinate location = new Coordinate(current.getX(), current.getY());
	    location.setDirection(current.getDirection());
	    
		Node bNode = new Node(view, location, 'b', current.getHaveAxe(), false, current.getHaveKey(), current.getHaveTreasure(), 
				current.getHaveRaft(), current.addedTool(), current.getOpenedDoor(), current.getOnWater());
		
		bNode.setG(cost);

		bNode.setF(bNode.getG() + getHeuristic(bNode));

		return bNode;
	}

	private Node getUNeighbour(Node current) {

		char inFront = '?';
		int inFrontX = getInFrontX(current.getAgentLocation(), current.getDirection());
		int inFrontY = getInFrontY(current.getAgentLocation(), current.getDirection());
		
		try{
			inFront = current.getState()[inFrontY][inFrontX];
		} catch (ArrayIndexOutOfBoundsException e){
			// leave inFront as '.' (out of bounds)
		}
		
		double cost = current.getG() + cheapCost;
		
		if(! current.getHaveKey() || ! (inFront == '-')){
			return current;
		}
		
		char[][] view = current.copyState(xMin, xMax, yMin, yMax);
		view[inFrontY][inFrontX] = ' ';
		
	    Coordinate location = new Coordinate(current.getX(), current.getY());
	    location.setDirection(current.getDirection());
	    
		Node uNode = new Node(view, location, 'u', current.getHaveAxe(), current.getHaveBomb(), current.getHaveKey(), current.getHaveTreasure(), 
				current.getHaveRaft(), current.addedTool(), true, current.getOnWater());
		
		uNode.setG(cost);
		uNode.setF(current.getG() + getHeuristic(uNode));
		return uNode;
	}
	
	private Node getCNeighbour(Node current) {
		
		char inFront = '?';
		int inFrontX = getInFrontX(current.getAgentLocation(), current.getDirection());
		int inFrontY = getInFrontY(current.getAgentLocation(), current.getDirection());
		
		try{
			inFront = current.getState()[inFrontY][inFrontX];
		} catch (ArrayIndexOutOfBoundsException e){
			// leave inFront as '?'
		}
		
		if(! current.getHaveAxe() || ! (inFront == 'T')){
			return current;
		}
		
		double cost = current.getG() + multiplyCost*moveCost;
		
		if(current.getHaveRaft() || onWater || current.getOnWater()) cost += multiplyCost*moveCost;
		char[][] view = current.copyState(xMin, xMax, yMin, yMax);
		view[inFrontY][inFrontX] = ' ';

	    Coordinate location = new Coordinate(current.getX(), current.getY());
	    location.setDirection(current.getDirection());

		Node cNode = new Node(view, location, 'c', current.getHaveAxe(), current.getHaveBomb(), current.getHaveKey(), current.getHaveTreasure(), 
				true, true, current.getOpenedDoor(), current.getOnWater());
		
		cNode.setG(cost);
		cNode.setF(cNode.getG() + getHeuristic(cNode));
		return cNode;
	}

	private Node getFNeighbour(Node current) {
		
		char inFront = '?';
		int inFrontX = getInFrontX(current.getAgentLocation(), current.getDirection());
		int inFrontY = getInFrontY(current.getAgentLocation(), current.getDirection());
		
		try{
			inFront = current.getState()[inFrontY][inFrontX];
		} catch (ArrayIndexOutOfBoundsException e){
			inFront = '.';
		}
		
		if( inFront == 0 || (inFront == '~' && ! current.getHaveRaft()) || inFront == '.' ) return null; // water or wilderness, so dead-end
		else if( inFront == '*' || inFront == 'T' || inFront == '-' ){
			return null;
		}
		
		boolean axe = current.getHaveAxe();
		boolean bomb = current.getHaveBomb();
		boolean key = current.getHaveKey();
		boolean treasure = current.getHaveTreasure();
		boolean raft = current.getHaveRaft();
		boolean onWater = current.getOnWater();
		
		boolean pickedUpTool = false;
		double cost = current.getG();

		if(inFront == '$'){
			treasure = true;
			cost = 0;
			if(treasureLocation == null) treasureLocation = new Coordinate(inFrontX, inFrontY);
		} else if(inFront == 'a' ){
			axe = true;
			pickedUpTool = true;
		} else if(inFront == 'k'){
			key = true;
			pickedUpTool = true;
		} else if(inFront == 'd'){
			bomb = true;
			pickedUpTool = true;
		} else if(inFront == '~'){
			onWater = true;
		} else if(inFront == ' '){
			if(haveTreasure && inFrontX == startLocation.getX() && inFrontY == startLocation.getY()) cost = 0;
			else cost += moveCost;
		}
		
		if(inFront != '~' && current.getOnWater()){
			raft = false;
			onWater = false;
			cost += multiplyCost*moveCost;
		}
		
		
		char[][] view;
		
		if(pickedUpTool){
			view = current.copyState(xMin, xMax, yMin, yMax);
			view[current.getY()][current.getX()] = map[current.getY()][current.getX()];
			cost += cheapCost;
		} else{
			view = map;
		}

		Coordinate newLoc = new Coordinate(inFrontX, inFrontY);
		newLoc.setDirection(current.getDirection());
		
		Node fNode = new Node(view, newLoc, 'f', axe, bomb, key, treasure, 
				raft, pickedUpTool, current.getOpenedDoor(), onWater);
		
		fNode.setG(cost);
		fNode.setF(fNode.getG() + getHeuristic(fNode));
		return fNode;
	}

	private Node getRNeighbour(Node current) {

	    char newDirection = current.getDirection();
	    switch(current.getDirection()){
			case '^':
				newDirection = '>';
				break;
			case '>':
				newDirection = 'v';
				break;
			case 'v':
				newDirection = '<';
				break;
			case '<':
				newDirection = '^';
				break;
	    }
	    Double cost = current.getG() + moveCost;
	    
	    Coordinate location = new Coordinate(current.getX(), current.getY());
	    location.setDirection(newDirection);
	    
	   Node rNode = new Node(map, location, 'r', current.getHaveAxe(), current.getHaveBomb(), current.getHaveKey(), 
			   current.getHaveTreasure(), current.getHaveRaft(), current.addedTool(), current.getOpenedDoor(), current.getOnWater());
	   
	   rNode.setG(cost);
	   rNode.setF(rNode.getG() + getHeuristic(rNode));
	   return rNode;
	}

	private Node getLNeighbour(Node current) {
		char newDirection = current.getDirection();

	    switch(current.getDirection()){
			case '^':
				newDirection = '<';
				break;
			case '>':
				newDirection = '^';
				break;
			case 'v':
				newDirection = '>';
				break;
			case '<':
				newDirection = 'v';
				break;
	    }
	    
	    Double cost = current.getG() + moveCost;
	    
	    Coordinate location = new Coordinate(current.getX(), current.getY());
	    location.setDirection(newDirection);
	    
	    Node lNode = new Node(map, location, 'l', current.getHaveAxe(), current.getHaveBomb(), current.getHaveKey(), 
	    		current.getHaveTreasure(), current.getHaveRaft(), current.addedTool(), current.getOpenedDoor(), current.getOnWater());
	    
	    lNode.setG(cost);
	    lNode.setF(lNode.getG() + getHeuristic(lNode));
		return lNode;
	}
	
	
	/**
	 * rotateView rotates the given view <view> by the specified number of <rotations> (same view if % 4 == 0)
	 * @param view
	 * @param rotations
	 * @return rotatedView
	 */
	public char[][] rotateView(char[][]view, int rotations){
		char[][] rotatedView = new char[view.length][view.length];
		char[][] temp = view.clone();
		if(rotations % 4 == 0) return view;
		
		for(int r = 0; r < rotations; r++){
		    for (int y1 = 0; y1 < rotatedView.length; y1++) {
		        for (int x1 = 0; x1 < rotatedView.length; x1++) {
		        	rotatedView[y1][x1] = temp[rotatedView.length-x1-1][y1];
		        }
		    }
		    for (int y1 = 0; y1 < rotatedView.length; y1++) {
		        for (int x1 = 0; x1 < rotatedView.length; x1++) {
		        	temp[y1][x1] = rotatedView[y1][x1];
		        }
		    }
		}
		return rotatedView;
	}
	
	
	/**
	 * returns x value for position in front of agent, in direction of agent
	 * @param location
	 * @param direction
	 * @return x
	 */
	public int getInFrontX(Coordinate location, char direction){
		int inFrontX = 0;
		
		switch(direction){
			case '^':
				inFrontX = location.getX();
				break;
			case '>':
				inFrontX = location.getX() + 1;
				break;
			case 'v':
				inFrontX = location.getX();
				break;
			case '<':
				inFrontX = location.getX() - 1;
				break;
		}
		return inFrontX;
	}
	
	
	/**
	 * returns y value for position in front of agent, in direction of agent
	 * @param location
	 * @param direction
	 * @return y
	 */
	public int getInFrontY(Coordinate location, char direction){
		int inFrontY = 0;		
		switch(direction){
			case '^':
				inFrontY = location.getY() - 1;
				break;
			case '>':
				inFrontY = location.getY();
				break;
			case 'v':
				inFrontY = location.getY() + 1;
				break;
			case '<':
				inFrontY = location.getY();
				break;
		}
		return inFrontY;
	}
	
	
	/**
	 * returns x value for position 2 in front of agent, in direction of agent
	 * @param location
	 * @param direction
	 * @return x
	 */
	public int get2InFrontX(Coordinate location, char direction){
		int inFrontX = 0;
		
		switch(direction){
			case '^':
				inFrontX = location.getX();
				break;
			case '>':
				inFrontX = location.getX() + 2;
				break;
			case 'v':
				inFrontX = location.getX();
				break;
			case '<':
				inFrontX = location.getX() - 2;
				break;
		}
		return inFrontX;
	}
	
	
	/**
	 * returns y value for position 2 in front of agent, in direction of agent
	 * @param location
	 * @param direction
	 * @return y
	 */
	public int get2InFrontY(Coordinate location, char direction){
		int inFrontY = 0;		
		switch(direction){
			case '^':
				inFrontY = location.getY() - 2;
				break;
			case '>':
				inFrontY = location.getY();
				break;
			case 'v':
				inFrontY = location.getY() + 2;
				break;
			case '<':
				inFrontY = location.getY();
				break;
		}
		return inFrontY;
	}

	
	/**
	 * Heuristic method using Strategy Pattern
	 * @param start
	 * @return
	 */
	public int getHeuristic(Node start) {
		return h.getHeuristic(start, treasureLocation, startLocation, cheapCost);
	}
	
	/**
	 * function to update map with given view. Will return same map if view has no newly discovered locations
	 * @param view
	 */
	
	private void updateMap(char[][] view) {
		
		int yMin = this.agentLocation.getY() - 2;
		int yMax = this.agentLocation.getY() + 2;
		int xMin = this.agentLocation.getX() - 2;
		int xMax = this.agentLocation.getX() + 2;
		
		if(yMin < this.yMin) this.yMin = yMin;
		if(yMax > this.yMax) this.yMax = yMax;
		if(xMin < this.xMin) this.xMin = xMin;
		if(xMax > this.xMax) this.xMax = xMax;
		
		switch(agentLocation.getDirection()){
			case '^':
				break;
			case '>':
				view = rotateView(view, 1);
				break;
			case 'v':
				view = rotateView(view, 2);
				break;
			case '<':
				view = rotateView(view, 3);
				break;
		}

		int viewX = 0;
		int viewY = 0;
		for (int mapY = yMin; mapY <= yMax; mapY++) {
			viewX = 0;
			for (int mapX = xMin; mapX <= xMax; mapX++) {
				if(view[viewY][viewX] == 0 ) {
					map[mapY][mapX] = ' ';
					agentLocation.setX(mapX);
					agentLocation.setY(mapY);
				} 
				else map[mapY][mapX] = view[viewY][viewX];
				if(treasureLocation == null && map[mapY][mapX] == '$') treasureLocation = new Coordinate(mapX, mapY);
				viewX++;
			}
			viewY++;
		}

	}
	
	/**
	 * method used during coding to watch the map expand as it is discovered. Can be added after updateView in getAction
	 * @param map
	 * @return string
	 */
	public String toString(char[][] map){

		String string = "";
		string += "+";
		for(int y = xMin; y <= xMax; y++){
			string += "-";
		}
		string += "+\n";
		
		for(int z = yMin; z <= yMax ; z++){
			string += "|";
            for(int w = xMin; w <= xMax ; w++){
            	if(z == agentLocation.getY() && w == agentLocation.getX()) string += agentLocation.getDirection();
            	else if(map[z][w] == 0) string += ".";
            	else string += map[z][w];
            }
            string += "|\n";
        }
		string += "+";
		for(int y = xMin; y <= xMax; y++){
			string += "-";
		}
		string += "+\n";
		
		return string;
	}
	
	void print_view(char view[][]) {
		int i, j;

		System.out.println("\n+-----+");
		for (i = 0; i < 5; i++) {
			System.out.print("|");
			for (j = 0; j < 5; j++) {
				if ((i == 2) && (j == 2)) {
					System.out.print('^');
				} else {
					System.out.print(view[i][j]);
				}
			}
			System.out.println("|");
		}
		System.out.println("+-----+");
	}
	
	public static void main(String[] args) {
		InputStream in = null;
		OutputStream out = null;
		Socket socket = null;
		Agent agent = new Agent(new Heuristic1());
		char view[][] = new char[5][5];
		char action = 'F';
		int port;
		int ch;
		int i, j;

		if (args.length < 2) {
			System.out.println("Usage: java Agent -p <port>\n");
			System.exit(-1);
		}

		port = Integer.parseInt(args[1]);

		try { // open socket to Game Engine
			socket = new Socket("localhost", port);
			in = socket.getInputStream();
			out = socket.getOutputStream();
		} catch (IOException e) {
			System.out.println("Could not bind to port: " + port);
			System.exit(-1);
		}

		try { // scan 5-by-5 window around current location
			while (true) {
				for (i = 0; i < 5; i++) {
					for (j = 0; j < 5; j++) {
						if (!((i == 2) && (j == 2))) {
							ch = in.read();
							if (ch == -1) {
								System.exit(-1);
							}
							view[i][j] = (char) ch;
						}
					}
				}
				//agent.print_view(view); // COMMENT THIS OUT BEFORE SUBMISSION
				action = agent.get_action(view);
				out.write(action);
			}
		} catch (IOException e) {
			System.out.println("Lost connection to port: " + port);
			System.exit(-1);
		} finally {
			try {
				socket.close();
			} catch (IOException e) {
			}
		}

	}
}
